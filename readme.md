

#Setup Instructions

1. Change references to REM server throughout this repo i.e. akrem115.cdflab.cafex.com
2. This app makes use of the Client SDK therefore you will need to create a web app id in your WPF admin Gaetway config.
3. In session.php change config for your server i.e. webAppId, username, AED2.allowedTopic
4. Be sure username and AED2.allowedTopic are prefixed with "assist-" for use with standard agent console / finesse
5. Set hard-coded reference to correlationId / cid throughout to your own custom value i.e. assist-12345678
6. In video.html mage sure the address of your call object is the reachable agent address i.e. for jabber 'sip:1095@akrem115.cdflab.cafex.com' where 1095 is the extension
7. You will need to deploy to two separate php servers, on different subdomains (one for domain1 and one for domain2) - then update your link between the two accordingly.
